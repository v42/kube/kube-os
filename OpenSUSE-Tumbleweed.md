# OpenSUSE Tumbleweed

The [OpenSUSE Tumbleweed](https://get.opensuse.org/tumbleweed/) OS installation 
and configuration as the base OS for the `kube` nodes is documented here.
- This procedure was tested with the `openSUSE-Tumbleweed-DVD-x86_64-Snapshot20211122-Media.iso` DVD

## Installation

Boot from the [OpenSUSE Tumbleweed](https://get.opensuse.org/tumbleweed/) DVD:
- Select `Installation` from the boot menu and wait for the installer to start
- Accept the language, keyboard and license agreement
- Activate the online repositories (use the default selection)
- Select System Role: `Server`
- Select `Guided Setup` for the disk partitions:
  - Remove all existing partitions *EVEN* if not needed
  - Do *NOT* enable LVM or disk encryption
  - Use XFS, *NO* separate home partition, *DO* propose separate swap partition and do *NOT* enlarge to RAM size
- Configure the clock and time zone
- Create a new user named `kube`, preferably with the same password on all `kube` nodes.
- Do *NOT* use that same password for the system administrator
- Provide the password for the `root` user, preferably with the same `root` password on all `kube` nodes.

Check the installation settings before clicking the `Install` button.

## Packages

- Remove `firewalld` as it conflicts with the way `k3s` manages the networking rules
- Install `lvm2` as this package is required by Ceph to store its data
```
zypper remove firewalld
zypper install lvm2
```

## Configuration

To configure the hostname of the new system:
- Configure the fully qualified hostname in `/etc/hostname` 
- Configure both short and fully qualified entries in `/etc/hosts`
- Reboot the system to activate the configured hostname

## Automatic Updates

As user `root` copy [`os/zypper.dup`](os/zypper.dup) to 
`/root/kube/os/zypper.dup` with mode `0750`:
```
cp os/zypper.dup /root/kube/os/zypper.dup
chmod 0750 /root/kube/os/zypper.dup
```
Now create a `root` crontab entry to run this script once a week at `01:01`:
```
1 1 * * <n> /root/kube/os/zypper.dup >/root/kube/os/zypper.dup.log 2>&1
```
Replace `<n>` by the node number, so node 1 will update itself on Monday,
node 2 on Tuesday, etcetera.
