#!/usr/bin/perl
package v42:kube:os::monitor;
use warnings;
use strict;

if (($#ARGV < 0) || ($ARGV[0] !~ /^\d$/)) {
    print "Usage: $0 <node-number>\n";
    exit (3);

# check uptime
