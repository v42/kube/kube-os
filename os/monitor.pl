#!/usr/bin/perl
use warnings;
use strict;
#------------------------------------------------------------------------------
# This is the main monitoring script. It is responsible for executing all the
# "monitor-*.pl" scripts, sending the results to all other nodes, and merging
# the results reported by all other nodes.
#
# Each "monitor-*.pl" script generates one or more lines of output, one per
# check, reporting the result of those checks. The format of each line is:
#
#       <id>|<status>|<output>|<perfdata>
#
# This format is loosely based on the format used by Nagios(tm) but with some
# twists to make it more usable for the way these scripts are being used here:
#
#       <id>        is a short string used to identify the check
#       <status>    is a status value in the range 0-7 (see below)
#       <output>    is a human-readable message describing the result
#       <perfdata>  is machine-readable info (one or more key=value pairs)
#
# This self-monitoring solution does NOT support multi-line output like Nagios
# does. If more lines are needed to describe a result, the check should be
# split into multiple parts that are able to report the result in a oneliner.
#
#   <id>        for instance "uptime"
#
#   <status>    0   Informational: informational messages
#               1   Notice: normal but significant condition
#               2   Warning: warning conditions
#               3   Error: error conditions (non-urgent)
#               4   Critical: critical conditions (urgent)
#               5   Alert: action must be taken immediately
#               6   Emergency: system is unusable (panic)
#   
#   <output>    for instance ...
#
#   <perfdata>  for instance ...
#------------------------------------------------------------------------------



